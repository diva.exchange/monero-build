#
# Copyright (C) 2021 diva.exchange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
#

FROM debian:stable-slim

LABEL author="Konrad Baechler <konrad@diva.exchange>" \
  maintainer="Konrad Baechler <konrad@diva.exchange>" \
  name="monero-build" \
  description="Distributed digital value exchange upholding security, reliability and privacy" \
  url="https://diva.exchange"

RUN apt-get update \
  && apt-get install -y \
    git \
    build-essential \
    cmake \
    pkg-config \
    libssl-dev \
    libzmq3-dev \
    libunbound-dev \
    libsodium-dev \
    libunwind8-dev \
    liblzma-dev \
    libreadline6-dev \
    libldns-dev \
    libexpat1-dev \
    libpgm-dev \
    qttools5-dev-tools \
    libhidapi-dev \
    libusb-1.0-0-dev \
    libprotobuf-dev \
    protobuf-compiler \
    libudev-dev \
    libboost-chrono-dev \
    libboost-date-time-dev \
    libboost-filesystem-dev \
    libboost-locale-dev \
    libboost-program-options-dev \
    libboost-regex-dev \
    libboost-serialization-dev \
    libboost-system-dev \
    libboost-thread-dev \
    ccache \
    doxygen \
    graphviz \
  && git clone --recursive https://github.com/monero-project/monero \
  && cd monero \
  && git submodule init \
  && git submodule update \
  && git checkout release-v0.17 \
  && git submodule sync \
  && git submodule update

CMD [ "/bin/bash" ]
